from dataclasses import asdict

import torch
from catalyst import dl


class CustomRunner(dl.Runner):
    """Раннер для обучения модели метода metric learning."""

    def __init__(self, config, *args, **kwargs):
        self.config = config
        super().__init__(*args, **kwargs)

    def get_loggers(self):
        return {
            'wandb': dl.WandbLogger(project=self.config.project_name,
                                    name=self.config.exp_name, config=asdict(self.config))
        }

    def handle_batch(self, batch):
        if self.is_train_loader:
            x, y = batch
            q = torch.Tensor([True] * len(y))
        else:
            x, y, q = batch
        embedding, logits = self.model(x)
        loss = self.criterion(logits, y)
        self.batch_metrics.update({'loss': loss, })
        self.batch = {
            'features': x,
            'embeddings': embedding,
            'targets': y,
            'is_query': q,
            'logits': torch.nn.functional.softmax(logits),
        }
        if self.is_train_loader:
            loss.backward()
            self.optimizer.step()
            self.optimizer.zero_grad()
