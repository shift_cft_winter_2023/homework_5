import numpy as np
from catalyst.data.sampler import BatchBalanceClassSampler
from torch.utils.data import DataLoader
from torchvision import datasets

from src.schemas.config import Config


def get_dataloader(config: Config) -> dict:
    """Задание параметров конструктора.

    Args:
        config: конфигурации
    Returns:
        генераторы данных.
    """

    train_dataset = datasets.ImageFolder(
        config.paths.train_path, transform=config.train_transforms.get())
    valid_dataset = datasets.ImageFolder(
        config.paths.valid_path, transform=config.test_transforms.get())

    if config.dataset.is_query_size:
        def rand(): return np.random.choice([True, False], p=[
            1 - config.dataset.is_query_size, config.dataset.is_query_size])
        valid_dataset = [(image, label, rand())
                         for image, label in valid_dataset]

    if config.dataset.balance:
        sampler = BatchBalanceClassSampler(
            labels=[l for _, l in train_dataset], num_classes=config.num_classes, num_samples=2, num_batches=config.batch_size)
        train_dataloader = DataLoader(
            train_dataset,
            batch_sampler=sampler,
            num_workers=config.dataset.num_workers,
            pin_memory=config.dataset.pin_memory,
        ),
    else:
        train_dataloader = DataLoader(
            train_dataset,
            batch_size=config.batch_size,
            shuffle=config.dataset.shuffle_train_dataset,
            num_workers=config.dataset.num_workers,
            pin_memory=config.dataset.pin_memory,
        )

    return {
        'train': train_dataloader,
        'valid': DataLoader(
            valid_dataset,
            batch_size=config.batch_size,
            shuffle=config.dataset.shuffle_test_dataset,
            num_workers=config.dataset.num_workers,
            pin_memory=config.dataset.pin_memory,
        ),
    }
