import torch
from catalyst import dl

from src.models.builder_models import BuildModel
from src.pipeline.dataset import get_dataloader
from src.pipeline.runner import CustomRunner
from src.schemas.config import Config


def train(config: Config):
    """Функция обучения модели

    Args:
        config: конфигурации
    """

    runner = CustomRunner(config)
    model = BuildModel()(config.model_name, config.pretrained,
                         config.embedding_size, config.num_classes, config.end_layer.get())
    optimizer = config.optimizer.get(model)
    criterion = config.criterion.get()
    scheduler = config.scheduler.get(optimizer) if config.scheduler else None
    callbacks = config.callbacks.get() if config.callbacks else list()

    loaders = get_dataloader(config)
    runner.train(
        model=model,
        engine=dl.DeviceEngine('cuda' if torch.cuda.is_available() else 'cpu'),
        criterion=criterion,
        optimizer=optimizer,
        scheduler=scheduler,
        loaders=loaders,
        callbacks=callbacks,
        logdir=config.paths.logs_path,
        valid_loader='valid',
        num_epochs=config.epochs,
        valid_metric='auc',
        verbose=True,
        minimize_valid_metric=False,
        seed=config.seed,
    )
