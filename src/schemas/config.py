from dataclasses import dataclass
from typing import Optional

from src.schemas import schemas


@dataclass
class Config:
    """Набор конфигураций."""

    project_name: str
    model_name: str
    embedding_size: int
    num_classes: int
    exp_name: str
    pretrained: bool
    batch_size: int
    epochs: int
    seed: int
    learning_rate: float
    paths: schemas.Paths
    dataset: schemas.Dataset
    criterion: schemas.Criterion
    optimizer: schemas.Optimizer
    scheduler: Optional[schemas.Scheduler]
    train_transforms: schemas.Transforms
    test_transforms: schemas.Transforms
    callbacks: Optional[schemas.Callbacks]
    end_layer: schemas.EndLayer
