import importlib
from dataclasses import dataclass
from typing import Dict, List, Optional

import torch
from catalyst.core import callback

from src.utilits.mixins import (DependentMixin, DependentMixinWithoutParams,
                                IndependentMixin, TemplateSchema,
                                TemplateTransforms)


@dataclass
class Paths:
    """Директории с данными."""

    train_path: str
    test_path: str
    valid_path: str
    logs_path: str
    label_path: str


@dataclass
class Dataset:
    """Параметры датасета."""

    balance: bool
    shuffle_train_dataset: bool
    shuffle_test_dataset: bool
    num_workers:  int
    pin_memory: int
    is_query_size: float


@dataclass
class Criterion(TemplateSchema, IndependentMixin):
    """Параметры лосс-функции."""


@dataclass
class Optimizer(TemplateSchema, DependentMixin):
    """Параметры отпимизатора."""


@dataclass
class Scheduler(TemplateSchema, DependentMixinWithoutParams):
    """Параметры планировщика."""


@dataclass
class EndLayer(TemplateSchema, IndependentMixin):
    """Параметры оптимизатора функции потерь."""


@dataclass
class Transforms(TemplateTransforms):
    """Параметры аугментации и предобработки изображений."""


@dataclass
class Callbacks:
    """Параментры callback'ов."""

    module: str
    callbacks: dict

    def get(self) -> Optional[List[callback.Callback]]:
        """Вспомогательный метод для создания callback'ов.

        Returns:
            набор callback'ов.
        """

        module = importlib.import_module(self.module)
        callbacks = []
        for k, v in self.callbacks.items():
            v = v if v is not None else {}
            shell = v.pop('shell', False)
            if shell:
                _k = list(shell.keys())[0]
                _shell = getattr(module, list(shell.keys())[0])
                callback = _shell(getattr(module, k)(**v), **shell[_k])
            else:
                callback = getattr(module, k)(**v)
            callbacks.append(callback)
        return callbacks
