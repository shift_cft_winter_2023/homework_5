import numpy as np
from catalyst.core.callback import Callback, CallbackNode, CallbackOrder
from pytorch_metric_learning.utils.accuracy_calculator import \
    AccuracyCalculator
from sklearn.cluster import DBSCAN
from sklearn.metrics import confusion_matrix

from src.utilits.get_emberdding import get_all_embeddings


class AccuracyCallback(Callback):
    """Коллбэк для подсчета точности из pytorch_metrci_learning."""

    def __init__(self, prefix='accuracy_metric_learning', k=1):
        super().__init__(order=CallbackOrder.metric, node=CallbackNode.all)
        self.accuracy_calculator = AccuracyCalculator(
            include=('precision_at_1',), k=k)
        self.prefix = prefix

    def get_metrics(self, loader, model):
        embeddings, labels = get_all_embeddings(loader.dataset, model)
        labels = labels.squeeze(1)
        return self.accuracy_calculator.get_accuracy(embeddings, labels, embeddings, labels, False,)

    def on_epoch_end(self, runner: "IRunner"):
        if not runner.is_valid_loader:
            return

        accuracies = self.get_metrics(runner.loaders['valid'], runner.model)
        runner.epoch_metrics['valid'].update(accuracies)
        runner.loader_metrics.update(accuracies)
        self.accuracies = accuracies
