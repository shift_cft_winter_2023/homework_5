import torch.nn as nn
from torchvision import models

from src.models.resnet_models import ResnetModel


class BuildModel:
    """Класс инициализации модели."""

    def __call__(self, model_name: str, pretrained: bool, embedding_size: int, num_classes: int, end_layer: nn.Module) -> nn.Module:
        """Метод для инициализации модели.

        Args:
            model_name: имя модели
            pretrained: флаг использования предобученной модели
            embedding_size: размер эмбеддинга
            num_classes: кол-во классов.
        Returns:
            модель.
        """
        if 'resnet' in model_name:
            model = getattr(models, 'resnet18')(pretrained=pretrained)
        else:
            raise Exception('This model is not define in zoo.')
        return ResnetModel(model, embedding_size=embedding_size, num_classes=num_classes, end_layer=end_layer)
