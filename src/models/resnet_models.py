import torch
import torch.nn as nn


class ResnetModel(nn.Module):
    """Класс создания для модели."""

    def __init__(self, model: nn.Module, embedding_size: int, num_classes: int, end_layer: nn.Module):
        """Задание параметров конструктора.

        Args:
            model: model
            embedding_size: размер эмбеддинга
            num_classes: кол-во классов.
        """

        super().__init__()
        self.model = model
        self.model.fc = nn.Linear(self.model.fc.in_features, embedding_size)
        self.end_layer = end_layer

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.model(x)
        y = self.end_layer(x)
        return x, y
