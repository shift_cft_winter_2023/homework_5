import importlib
from dataclasses import dataclass, field
from typing import Dict, Optional


@dataclass
class TemplateTransforms:
    """Параметры аугментации и предобработки изображений."""

    module: str
    transforms: Optional[dict] = field(default_factory=dict)

    def _get_default_transform(self, module):
        return [getattr(module, 'ToTensor')()]

    def get(self):
        module = importlib.import_module(self.module)
        transforms = self._get_default_transform(module)
        for transform in self.transforms:
            transforms.append(getattr(module, transform)
                              (**self.transforms[transform]))
        return module.Compose(transforms)


@dataclass
class TemplateSchema:
    """Шаблон для схемы."""

    module: str
    model: str
    args: Optional[dict]


class IndependentMixin:
    """Вспомогательный миксин для создания независимых объектов."""

    def get(self):
        module = importlib.import_module(self.module)
        if self.args is None:
            return getattr(module, self.model)()
        return getattr(module, self.model)(**self.args)


class DependentMixin:
    """Вспомогательный миксин для создания зависимых объектов."""

    def get(self, depend):
        module = importlib.import_module(self.module)
        if self.args is None:
            return getattr(module, self.model)(depend.parameters(),)
        return getattr(module, self.model)(depend.parameters(), **self.args)


class DependentMixinWithoutParams:
    """Вспомогательный миксин для создания зависимых объектов."""

    def get(self, depend):
        module = importlib.import_module(self.module)
        if self.args is None:
            return getattr(module, self.model)(depend,)
        return getattr(module, self.model)(depend, **self.args)
