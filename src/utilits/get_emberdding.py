import torch.nn as nn
from pytorch_metric_learning import testers
from torch.utils.data import Dataset


def get_all_embeddings(dataset: Dataset, model: nn.Module) -> tuple:
    """Вспомогательный метод для создания отпимизатора.

    Args:
        dataset: набор данных
        model: обученная модель
    Returns:
        эмбеддинг
     """
    tester = testers.BaseTester()
    return tester.get_all_embeddings(dataset, model,)
