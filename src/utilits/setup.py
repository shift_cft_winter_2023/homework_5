import os
import random
from pathlib import Path
from warnings import filterwarnings

import numpy as np
import torch
import yaml
from dacite import from_dict

from src.schemas.config import Config


def set_warnings():
    """Настройка предупреждений."""

    filterwarnings('ignore')


def set_seed(seed: int):
    """Установка сидов."""

    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.benchmark = True


def init_config(path: str) -> Config:
    """Инициализация config.

    Args:
        path: путь до файла с конфигом.
    Returns:
        Класс с конфигурациями.
    """

    config = yaml.safe_load(Path(path).read_text())
    config = from_dict(data_class=Config, data=config)
    return config
