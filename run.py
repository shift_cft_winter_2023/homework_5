from argparse import ArgumentParser

from src.pipeline.train import train
from src.utilits.setup import init_config, set_seed, set_warnings


def parse_arguments():
    args_parser = ArgumentParser()
    args_parser.add_argument(
        '--config', '-c',
        dest='config',
        help='Path to config',
        required=True,
        default='config/config.yaml',
    )
    args_parser.add_argument(
        '--mode', '-m',
        dest='mode',
        help='Mode of run',
        required=True,
        choices=['train', 'predict',],
    )
    args_parser.add_argument(
        '--path_to_image', '-pti',
        dest='path_to_image',
        help='Path to image for mode=predict',
        required=False,
    )
    args_parser.add_argument(
        '--path_to_model', '-ptm',
        dest='path_to_model',
        help='Path to pretrain model for mode=predict',
        required=False,
    )
    return args_parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()

    config = init_config(args.config)
    set_seed(config.seed)
    set_warnings()

    if args.mode == 'train':
        train(config)
